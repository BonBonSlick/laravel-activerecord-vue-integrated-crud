<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEventApplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() : void
    {
        Schema::create('event_applications', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('event_id')->comment('Event for which we get our guests together.Another entity');

            $table->string('name');
            $table->string('phone');
            $table->string('inviting_country');
            $table->string('inviting_country_weather');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() : void
    {
        Schema::dropIfExists('event_applications');
    }
}
