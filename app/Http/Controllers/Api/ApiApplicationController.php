<?php
declare(strict_types = 1);
/**
 * @SWG\Post(
 *     path="/api/submit",
 *     summary="Save event application.",
 *     tags={"Event"},
 *     description="Saves applicants for event.",
 *     operationId="api.event.submit",
 *     produces={"application/json"},
 *     @SWG\Parameter(
 *         in="query",
 *         required=false,
 *         description="Array of names and phones",
 *         type="array"
 *          @SWG\Items(
 *              @SWG\Property(property="name", type="string", ),
 *              @SWG\Property(property="phone", type="string",),
 *           ),
 *     ),
 *     @SWG\Response(response=200, description="Blog Items list."),
 * )
 *
 */

namespace App\Http\Controllers\Api;

use App\EventApplication;
use App\Http\Controllers\Controller;
use App\Services\GeolocationService;
use App\Services\WeatherService;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use Illuminate\Validation\ValidationException;
use Throwable;

class ApiApplicationController extends Controller
{
    /**
     * @var GeolocationService
     */
    private $geolocationService;

    /**
     * @var WeatherService
     */
    private $weatherService;

    /**
     * ApiApplicationController constructor.
     *
     * @param GeolocationService $geolocationService
     * @param WeatherService     $weatherService
     */
    public function __construct(GeolocationService $geolocationService, WeatherService $weatherService)
    {
        $this->geolocationService = $geolocationService;
        $this->weatherService     = $weatherService;
    }

    /**
     * Saves event applications to DB
     *
     * @param Request $request
     *
     * @return JsonResponse
     */
    public function submit(Request $request) : ?JsonResponse
    {
        $country        = $this->geolocationService->currentRequestCountry($request);
        $weatherDetails = 'Unknown country';
        if ($country) {
            $weatherDetails = $this->weatherService->countryWeather($country);
        }
        try {
            $this->validate(
                $request,
                [
                    '*.name'  => 'required|string|max:99',
                    '*.phone' => 'required|string|max:25',
                ],
                [
                    '*.name.required'  => 'Name is required',
                    '*.phone.required' => 'Phone is required',
                ]
            );
            DB::beginTransaction();
            $eventId = \random_int(
                0,
                9999
            ); // some dummy event id, we apply for
            foreach ($request->all() as $applicationData) {
                $applicationData['event_id']                 = $eventId;
                $applicationData['inviting_country']         = $country;
                $applicationData['inviting_country_weather'] = $weatherDetails;
                EventApplication::create($applicationData);
            }
            DB::commit();

            return response()->json(
                [
                    'created' => true,
                    'message' => 'Successfully applied!',
                ]
            );
        }
        catch (ValidationException $exception) {
            DB::rollBack();

            return response()->json(
                [
                    'errors'     => true,
                    'violations' => $exception->validator->errors()->messages(),
                    'message'    => $exception->getMessage(),
                ]
            );
        }
        catch (Throwable $exception) {
            DB::rollBack();

            return response()->json(
                [
                    'errors'  => true,
                    'message' => $exception->getMessage(),
                ]
            );
        }
    }
}