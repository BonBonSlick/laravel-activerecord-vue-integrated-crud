<?php
declare(strict_types = 1);

namespace App\Services;

use Illuminate\Http\Request;

class GeolocationService
{
    /**
     * @param Request $request
     *
     * @return null|string
     */
    public  function currentRequestCountry(Request $request) : ?string
    {
        $ip            = $request->ip();
        $ipInfo = json_decode(file_get_contents("http://ipinfo.io/{$ip}"));
        return $ipInfo->country ?? 'Unknown';
    }
}