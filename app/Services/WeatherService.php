<?php
declare(strict_types = 1);

namespace App\Services;

use stdClass;

class WeatherService
{
    /**
     * Info about current weather in country
     *
     * @param string|null $country
     *
     * @return string
     */
    public function countryWeather($country)
    {
        if ($country === null) {
            return new stdClass();
        }
        $weatherSecret = env('WEATHER_API_KEY');

        $countryWeather = json_decode(
            file_get_contents("http://api.openweathermap.org/data/2.5/weather?q={$country}&appid={$weatherSecret}")
        );

        $weatherMainInfo       = isset($countryWeather->weather->main) ? : 'Unknown weather';
        $weatherAdditionalInfo = isset($countryWeather->weather->description )? : 'Unknown weather';

        return 'General: ' . $weatherMainInfo . ' Details: ' . $weatherAdditionalInfo;
    }
}